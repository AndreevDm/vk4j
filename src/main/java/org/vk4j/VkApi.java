package org.vk4j;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.vk4j.wall.WallGetFilter;
import org.vk4j.wall.WallPosts;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Andreev <a href="mailto:AndreevDm@gmail.com"/>
 * @date 15.04.2013
 */
public class VkApi {

    private HttpClient httpClient;
    private VkConfig vkConfig;
    private BasicNameValuePair accessToken;
    private Gson gson = new Gson();

    public VkApi(VkConfig vkConfig, HttpClient httpClient) {
        this.httpClient = httpClient;
        this.vkConfig = vkConfig;
        this.accessToken = new BasicNameValuePair("access_token", vkConfig.getAccessToken());
    }

    public VkApi(VkConfig vkConfig) {
        this(vkConfig, new DefaultHttpClient(new ThreadSafeClientConnManager()));
    }

    protected JsonElement executeGetRequest(String methodName, String... params) throws IOException {
        String urlParams = createParams(params);
        HttpGet httpGet = new HttpGet("https://api.vk.com/method/" + methodName + "?" + urlParams);
        return executeRequest(httpGet);
    }

    protected JsonElement executeRequest(HttpUriRequest httpUriRequest) throws IOException {
        return executeRequest(httpUriRequest, true);
    }

    protected JsonElement executeRequest(HttpUriRequest httpUriRequest, boolean extractResponse) throws IOException {
        HttpResponse httpResponse = httpClient.execute(httpUriRequest);
        if (httpResponse.getStatusLine().getStatusCode() != 200) {
            throw new IOException(
                    "Bad http code (" + httpResponse.getStatusLine().toString() + ") for request" +
                            httpUriRequest.toString()
            );
        }
        JsonElement jsonResponse = gson.fromJson(
                new InputStreamReader(httpResponse.getEntity().getContent()),
                JsonObject.class
        );
        if (extractResponse) {
            return jsonResponse.getAsJsonObject().get("response");
        } else {
            return jsonResponse;
        }
    }

    protected String createParams(String... stringParams) {
        if (stringParams.length % 2 != 0) {
            throw new IllegalArgumentException("Params count must be even");
        }
        int count = stringParams.length / 2;
        List<NameValuePair> params = new ArrayList<NameValuePair>(count + 1);
        for (int i = 0; i < stringParams.length; i += 2) {
            params.add(new BasicNameValuePair(stringParams[i], stringParams[i + 1]));
        }
        params.add(accessToken);
        return URLEncodedUtils.format(params, "UTF-8");
    }

    //Users methods
    public int getUserSettings() throws IOException {
        return executeGetRequest(
                "getUserSettings"
        ).getAsInt();
    }


    //Wall methods

    public WallPosts wallGet(WallGetFilter wallGetFilter, int count) throws IOException {
        JsonArray jsonPosts = executeGetRequest(
                "wall.get",
                "filter", wallGetFilter.name(),
                "count", Integer.toString(count)
        ).getAsJsonArray();
        return new WallPosts(jsonPosts);
    }

    public int wallPost(String text, String... attachments) throws IOException {
        JsonElement response;
        if (attachments.length == 0) {
            response = executeGetRequest("wall.post", "message", text);
        } else {
            response = executeGetRequest(
                    "wall.post",
                    "message", text,
                    "attachments", buildAttachmentsString(attachments)
            );
        }
        return response.getAsJsonObject().getAsJsonPrimitive("post_id").getAsInt();
    }

    private String buildAttachmentsString(String... attachments) {
        StringBuilder attachmentsBuilder = new StringBuilder();
        for (int i = 0; i < attachments.length; i++) {
            if (i != 0) {
                attachmentsBuilder.append(",");
            }
            attachmentsBuilder.append(attachments[i]);
        }
        return attachmentsBuilder.toString();
    }

    public String photosGetWallUploadServer() throws IOException {
        JsonElement jsonElement = executeGetRequest("photos.getWallUploadServer", "save_big", "1");
        return jsonElement.getAsJsonObject().get("upload_url").getAsString();
    }

    //Photos methods

    public JsonObject saveWallPhoto(String server, String photo, String hash) throws IOException {
        JsonElement jsonElement = executeGetRequest(
                "photos.saveWallPhoto",
                "server", server,
                "photo", photo,
                "hash", hash
        );
        return jsonElement.getAsJsonArray().get(0).getAsJsonObject();
    }


}
