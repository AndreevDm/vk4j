package org.vk4j.wall;

/**
 * @author Dmitry Andreev <a href="mailto:AndreevDm@gmail.com"/>
 * @date 14.05.13
 */
public enum WallGetFilter {
    OWNER,
    OTHERS,
    All
}
