package org.vk4j.wall;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author Dmitry Andreev <a href="mailto:AndreevDm@gmail.com"/>
 * @date 14.05.13
 */
public class WallPost {
    private int id;
    private String text;
    private String urlAttachment;

    public WallPost(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public WallPost(JsonObject jsonPost) {
        id = jsonPost.get("id").getAsInt();
        text = jsonPost.get("text").getAsString();
        parseAttachments(jsonPost.get("attachments"));
    }

    private void parseAttachments(JsonElement jsonElement) {
        if (jsonElement == null) {
            return;
        }
        JsonArray jsonAttachments = jsonElement.getAsJsonArray();
        for (JsonElement jsonAttachment : jsonAttachments) {
            JsonObject jsonObject = jsonAttachment.getAsJsonObject();
            if (jsonObject.get("type").getAsString().equals("link")) {
                urlAttachment = jsonObject.getAsJsonObject("link").get("url").getAsString();
            }
        }
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getUrlAttachment() {
        return urlAttachment;
    }
}
