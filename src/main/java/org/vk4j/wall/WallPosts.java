package org.vk4j.wall;

import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Dmitry Andreev <a href="mailto:AndreevDm@gmail.com"/>
 * @date 14.05.13
 */
public class WallPosts {
    private int totalPosts;
    private List<WallPost> posts;

    public WallPosts(int totalPosts, List<WallPost> posts) {
        this.totalPosts = totalPosts;
        this.posts = posts;
    }

    public WallPosts(JsonArray jsonPosts) {
        totalPosts = jsonPosts.get(0).getAsInt();
        if (jsonPosts.size() == 1) {
            posts = Collections.emptyList();

        } else {
            posts = new ArrayList<WallPost>(jsonPosts.size() - 1);
            for (int i = 1; i < jsonPosts.size(); i++) {
                posts.add(new WallPost(jsonPosts.get(i).getAsJsonObject()));
            }
        }
    }

    public int getCount(){
        return posts.size();
    }

    public int getTotalPosts() {
        return totalPosts;
    }

    public List<WallPost> getPosts() {
        return posts;
    }
}
