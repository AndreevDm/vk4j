package org.vk4j;

import com.google.gson.JsonObject;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Dmitry Andreev <a href="mailto:AndreevDm@gmail.com"/>
 * @date 14.05.13
 */
public class VkApiExtended extends VkApi {

    public VkApiExtended(VkConfig vkConfig, HttpClient httpClient) {
        super(vkConfig, httpClient);
    }

    public VkApiExtended(VkConfig vkConfig) {
        super(vkConfig);
    }

    public String uploadWallPhoto(File photo) throws IOException {
        String uploadUrl = photosGetWallUploadServer();
        JsonObject jsonObject = uploadFile(uploadUrl, "photo", photo);
        return jsonObject.getAsJsonPrimitive("id").getAsString();
    }

    public String uploadWallPhoto(InputStream inputStream, long contentLength) throws IOException {
        String uploadUrl = photosGetWallUploadServer();
        JsonObject jsonObject = uploadInputStream(uploadUrl, "photo", inputStream, contentLength, "image.jpg");
        return jsonObject.getAsJsonPrimitive("id").getAsString();
    }

    public JsonObject uploadInputStream(String url, String field, final InputStream inputStream,
                                        final long contentLength, String fileName) throws IOException {
        ContentBody contentBody = new InputStreamBody(inputStream, fileName) {
            @Override
            public long getContentLength() {
                return contentLength;
            }
        };
        return uploadContentBody(url, field, contentBody);
    }

    public JsonObject uploadFile(String url, String field, File file) throws IOException {
        ContentBody contentBody = new FileBody(file, "multipart/form-data");
        return uploadContentBody(url, field, contentBody);
    }

    public JsonObject uploadContentBody(String url, String field, ContentBody contentBody) throws IOException {
        HttpPost httpPost = new HttpPost(url);
        MultipartEntity multipartEntity = new MultipartEntity();
        multipartEntity.addPart(field, contentBody);
        httpPost.setEntity(multipartEntity);
        JsonObject uploadResponse = executeRequest(httpPost, false).getAsJsonObject();
        JsonObject jsonObject = saveWallPhoto(
                uploadResponse.getAsJsonPrimitive("server").getAsString(),
                uploadResponse.getAsJsonPrimitive("photo").getAsString(),
                uploadResponse.getAsJsonPrimitive("hash").getAsString()
        );
        return jsonObject;
    }
}
